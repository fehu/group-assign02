#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "pico/stdlib.h"
#include "hardware/pio.h"
#include "hardware/clocks.h"
#include "hardware/watchdog.h"
#include "assign02.pio.h"
#include "hardware/pwm.h"
#include <time.h>

#define IS_RGBW true                // Will use RGBW format
#define NUM_PIXELS 1                // There is 1 WS2812 device in the chain
#define WS2812_PIN 28               // The GPIO pin that the WS2812 connected to
#define ARRAY_SIZE 173

int morse = 1;                      // When 1 displays morse
int letter = 1;                     // When 1 displays letter
int Num_Victories = 0;              // Number of Victories
int Num_Lives = 3;                  // Lives

// Used for calculates Stats function

int correct_input = 0;
int incorrect_input = 0;

// Used for add input function 

char curr_input[200];
int curr_index= -1;
absolute_time_t start_time;         //for alarm timer

typedef struct Morse Morse;
struct Morse
{
    char morse_name[20];            // max string size
    char letter;
};


// Generates morse code for each word & letter that the user will input
 

Morse* Morse_Gen(char* name, char letter)
{
    Morse* new = (Morse*) malloc(sizeof(Morse));
    strcpy(new->morse_name, name);
    new->letter = letter;
    return new;
}


// Stores the random letter generated for the New Morse function.

Morse* Random_Letter[36];
char Random_Arr[36] = "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
char letters[36] = "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";


// Stores the corresponding morse values for the aplahbets from A to Z and numbers from 0-9.


char morse_chars[36][6] = 
{
    {".-"}, {"-..."},{"-.-."}, {"-.."}, {"."},{"..-."}, {"--."},{"...."}, {".."}, {".---"}, {"-.-"},
    {".-.."}, {"--"}, {"-."}, {"---"}, {".--."},  {"--.-"}, {".-."},  {"..."}, {"-"},  {"..-"}, {"...-"},
    {".--"}, {"-..-"}, {"-.--"},  {"--.."}, {".----"}, {"..---"}, {"...--"}, {"....-"}, {"....."}, {"-...."},
    {"--..."},  {"---.."}, {"----."}, {"-----"},
};


// Function to generate random letters/words and it's morse  equivalent to what the user inputs
 

void New_Morse()
{
    for(int i = 0; i < 36; i ++)
    {
        Random_Letter[i] = Morse_Gen(morse_chars[i], letters[i]);
    }
}


// Simple array swap function thatll be used in the random permutation generator


void array_Swap(int x, int y)
{ 
    Morse* temp = Random_Letter[x];
    Random_Letter[x] = Random_Letter[y];
    Random_Letter[y] = temp;
}


// Function to generate the random permutation for the finite sequence.
 

void RandomPermutation()
{
    New_Morse();
    srand(time(NULL));
    absolute_time_t time = get_absolute_time();
    int RandGenerator = to_ms_since_boot(time);
    srand(RandGenerator);
    for(int i = 0; i < 36; i++)
    {
        int x = rand() % 36;
        array_Swap(i, x);
    }
}

Morse* hashTable[ARRAY_SIZE];

int hash_func(char* s)
{
    int hash = 0;
    while(*s)
    {
        hash = 31 * (hash + *s) % ARRAY_SIZE;
        s++;
    }
    return hash;
}


// Function used to search the hash table by calling hash_func
  
int hash_search (char* name)
{
    int hashNumber = hash_func(name);
    int startPoint = hashNumber;
    while(hashTable[hashNumber] != NULL)
    {
        if(strcmp(hashTable[hashNumber]->morse_name, name)==0)
        {
            return hashNumber;
        }
        hashNumber++;
        if(hashNumber == ARRAY_SIZE){
            hashNumber = 0;
        }
        if(hashNumber == startPoint)
        {
            return -1;
        }
    }
    return hashNumber;
}


// Function initialises the hash table by inserting the alphanumeric values into the hash table till the key values of hash table gets exhausted (will never happen).
 
void insert()
{
    int i = 0;
    while(i < 36)
    {
        int hashNumber = hash_search(morse_chars[i]);
        if(hashNumber != -1)
        {
            Morse* new_morse = Morse_Gen(morse_chars[i], letters[i]);
            hashTable[hashNumber] = new_morse;
        }
        else
        {
            printf("Hash table is full.");
        }
        i++;
    }
}


// This function starts the timer when the alarm interupt is called.
 
void timer_begin()
{
    start_time = get_absolute_time();
}


// This function returns an int which is the time elapsed of the alarm interupt when the button is pressed. 

int timer_end()
{
    int endTime = (int) absolute_time_diff_us(start_time, get_absolute_time());
    return endTime/100000;
}

// The add_input function is called on in many subroutines to add the input as a dot, dash, space, or null charcater.
// It will also increment an index counter each time the function is called to keep track of how many characters are being added.   

void add_input(int temp, int index)
{
    curr_index = curr_index - index;
    if (curr_index < 200)
    {
        if (temp == 0)
        {
            curr_input[curr_index] = '.';
        }
        else if (temp == 1)
        {
            curr_input[curr_index] = '-';
        }
        else if (temp == 2)
        {
            curr_input[curr_index] = ' ';
        }
        else if(temp == 3)
        {
            curr_input[curr_index] = '\0';
        }
    }
    curr_index +=  1;
    if (index)
    {
        printf("\b%c",curr_input[curr_index-1]);
    }
    else printf("%c",curr_input[curr_index-1]);

}

// Must declare the main assembly entry point before use.

void main_asm();


// Initialise a GPIO pin

void asm_gpio_init(uint pin)
{
    gpio_init(pin);
}

// Set direction of a GPIO pin

void asm_gpio_set_dir(uint pin, bool out) 
{
    gpio_set_dir(pin, out);
}


// Get the value of a GPIO pin 

bool asm_gpio_get(uint pin)
{
    return gpio_get(pin);
}


// Set the value of a GPIO pin

void asm_gpio_put(uint pin, bool value) 
{
    gpio_put(pin, value);
}


// Set the falling and rising edge interrupts for the pin specified

void asm_gpio_set_irq(uint pin)
{
    gpio_set_irq_enabled(pin, GPIO_IRQ_EDGE_RISE | GPIO_IRQ_EDGE_FALL, true);
}


// Function used to call the underlying PIO function that pushes the 32-bit RGB colour value
// out to the LED using the PIO0 block. The function only returns when all the data has been written out
  
static inline void pixel_add(uint32_t pixel_grb) 
{
    pio_sm_put_blocking(pio0, 0, pixel_grb << 8u);
}

/**
 * Function that generate an unsigned 32-bit composit RGB
 * value by combining the paramaters for
 * red, green and blue together in the right order.
 * 
 * @param r The 8-bit intensity value for the red component
 * @param g The 8-bit intensity value for the green component
 * @param b The 8-bit intensity value for the blue component
 */

static inline uint32_t rgb_gen(uint8_t r, uint8_t g, uint8_t b) 
{
    return  ((uint32_t) (r) << 8)  | ((uint32_t) (g) << 16) | (uint32_t) (b);
}



// Set LED in red, blue,orange, green, yellow

void set_LED_red()
{
    pixel_add(rgb_gen(0x7F, 0x00, 0x00));
}

void set_LED_blue()
{
    pixel_add(rgb_gen(0x00, 0x00, 0x7F));
}

void set_LED_orange()
{
    pixel_add(rgb_gen(0xFF, 0xA5, 0x00));
}

void set_LED_green()
{
    pixel_add(rgb_gen(0x00, 0x7F, 0x00));
}

void set_LED_yellow()
{
    pixel_add(rgb_gen(0x0F,0x0F,0x0F)); 
}

// Sets LED off. 

void set_LED_off()
{
    pixel_add(rgb_gen(0x00, 0x00, 0x00));
}

// Set Different colours according to life value     

void set_correct_led()
{
    if (Num_Lives == 1)
    {
        set_LED_yellow();
    }
    else if (Num_Lives == 2)
    {
        set_LED_orange();
    }
    else
    {
        set_LED_green();
    } 
    return;
}

// Show the statistics at the end of each level.

void statistics_table(int temp)
{
    int total_input = correct_input + incorrect_input;
    printf("\n+-----------------------------------------------------------------------------------------+\n");
    printf("\n                                          Statistics:                                      \n");
    printf("\n                                        Total_Attemps: %d                                  \n", total_input);
    printf("\n                                           Correct: %d                                     \n", correct_input);
    printf("\n                                            Wrong: %d                                      \n", incorrect_input);
    printf("\n                                             Win: %d                                       \n", Num_Victories);
    printf("\n                                        Remain Lives: %d                                   \n", Num_Lives);
    printf("\n+-----------------------------------------------------------------------------------------+\n");
    if (correct_input!=0 || incorrect_input!=0)
    {
        int total_accuary = (correct_input / total_input) * 100;
        if (temp)
        {
            printf("\n\t\tCorrect %% for this level: \t%.2d%%*\n",total_accuary); 
            correct_input = 0;
            incorrect_input = 0;
        }
        else 
        {
            printf("\n\t\tCorrect Percent :\t\t\t%.2d%%*\n",total_accuary);
        }
    }
    printf("\n+-----------------------------------------------------------------------------------------+\n");
}

// Select the level according to the input 

int select_level(int level)
{
    if (level == 1)
    {
        morse = 1;
        letter = 1;
    }
    else if (level == 2)
    {
        morse = 0;
        letter = 1;
    }
    else if (level == 3)
    {
        morse = 1;
        letter = 0;
    }
    else if (level == 4)
    {
        morse = 0;
        letter = 0;
    }
    return level;
}


void learn_morse()
{
    set_LED_blue();

    printf("\n+-----------------------------------------------------------------------------------------+\n");
    printf("\n|                     Made by group 38 for Microprocessor Assignment 2                    |\n");
    printf("\n|                    by: Eshaan, Ellen, Fengchen, Fiyinfoluwa and Eliel                   |\n");
    printf("\n+-----------------------------------------------------------------------------------------+\n");
    printf("\n|                #         #######       #         ######        #      #                 |\n");
    printf("\n|                #         #            # #        #     #       ##     #                 |\n");
    printf("\n|                #         #           #   #       #     #       # #    #                 |\n");
    printf("\n|                #         #####      #     #      ######        #  #   #                 |\n");
    printf("\n|                #         #          #######      #   #         #   #  #                 |\n");
    printf("\n|                #         #          #     #      #    #        #     ##                 |\n");
    printf("\n|                #######   #######    #     #      #     #       #      #                 |\n");
    printf("\n|                                                                                         |\n");
    printf("\n|                #     #   #######    ######        ######       #######                  |\n");
    printf("\n|                ##   ##   #     #    #     #      #      #      #                        |\n");
    printf("\n|                # # # #   #     #    #     #      #             #                        |\n");
    printf("\n|                #  #  #   #     #    ######        #####        #####                    |\n");
    printf("\n|                #     #   #     #    #   #              #       #                        |\n");
    printf("\n|                #     #   #     #    #    #       #     #       #                        |\n");
    printf("\n|                #     #   #######    #     #       #####        #######                  |\n");
    printf("\n+-----------------------------------------------------------------------------------------+\n");
    printf("\n|                          USE GP21 TO ENTER A SEQUENCE TO BEGIN                          |\n");
    printf("\n|                           \"-----\" - LEVEL 01 - CHARS (EASY)                             |\n");
    printf("\n|                           \".----\" - LEVEL 02 - CHARS (HARD)                             |\n");
    printf("\n|                           \"..---\" - LEVEL 03 - WORDS (HARD)                             |\n");
    printf("\n|                           \"...--\" - LEVEL 04 - WORDS (HARD)                             |\n");
    printf("\n+-----------------------------------------------------------------------------------------+\n");
    
    curr_index = -1;
    main_asm();
    printf("\n\n");

    int curr_level;
    if (strcmp(curr_input, "-----") == 0) 
    {
        curr_level = select_level(1);
    }
    else if (strcmp(curr_input, ".----") == 0) 
    {
        curr_level = select_level(2);
    }
    else if (strcmp(curr_input, "..---") == 0) 
    {
        curr_level = select_level(3);
    }
    else if (strcmp(curr_input, "...--") == 0) 
    {
        curr_level = select_level(4);
    }
    else 
    {
        printf("\n+-----------------------------------------------------------------------------------------+\n");
        printf("\n                            Inavlid input!!! Program will exit now                         \n");
        printf("\n+-----------------------------------------------------------------------------------------+\n");
        return;
    }

    set_correct_led();
    int random_count = rand() % 36;
    char curr_char;
    char* curr_morse_code;
    char curr_word[4];
    char curr_morse_code2[50] = "";
    char space[2] = " \0";

    while(Num_Lives > 0) 
    {
        printf("\n\n");

        if(letter == 1) 
        {
            curr_char = Random_Letter[random_count]->letter;
            curr_morse_code = Random_Letter[random_count]->morse_name;

            if(morse == 1) 
            { 
                printf("\nYour question is: %c \nand %c in Morse code that is reprsented by %s:\n", curr_char, curr_char, curr_morse_code);
            }
            else
                printf("\nYour question is: %c\n", curr_char);
        }
        else 
        {
            for (int i = 0; i < 3; i++)
            {
                curr_word[i] = Random_Letter[random_count]->letter;
                strcat(curr_morse_code2, Random_Letter[random_count]->morse_name);
                if(i != 2) 
                {
                    strcat(curr_morse_code2, space);
                }
                random_count = rand() % 36;
            }
            curr_word[3] = '\0';
            curr_morse_code = curr_morse_code2;
            if(morse == 1) 
            {
                printf("\nYour question is: %c \n and %c in Morse code that is reprsented by %s:\n", curr_word, curr_word, curr_morse_code);
            }
            else
                printf("\nYour question is: %c\n", curr_word);
        }

        printf("\nEnter the Morse Code after the question\n");

        curr_index = -1;
        main_asm();
        if (curr_index == 0) 
        {
            printf("\nNo update !!! New level being generated\n");
            continue;
        }

        int position = hash_search(curr_input);
        if(strcmp(curr_morse_code, curr_input) == 0) 
        {
            correct_input++;
            Num_Victories++;
            printf("\nCorrect answer was inputted!!!\n");
            if (Num_Lives < 3) 
            {
                Num_Lives += 1;
            }
            if (Num_Victories == 5) 
            {
                if (curr_level == 4) 
                {
                    printf("\n+-----------------------------------------------------------------------------------------+\n");
                    printf("\n                              The game has been completed!!!                               \n");
                    printf("\n+-----------------------------------------------------------------------------------------+\n");
                    break;
                }

                curr_level = select_level(curr_level + 1);
                printf("\n+-----------------------------------------------------------------------------------------+\n");
                printf("\n                                Going to new level %d                                      \n", curr_level);
                printf("\n+-----------------------------------------------------------------------------------------+\n");
                statistics_table(1);
                Num_Victories = 0;
            }
        
        }
        else 
        {
            incorrect_input++;
            printf("Incorrect answer inputted:\n");
            if (strcmp(hashTable[position]->morse_name, "") == 0) 
            {
                printf("\n The Morse Code entered does not exsist\n");
            }
            else if (letter) 
            {
                printf("\n The Morse Code inputted by you is %c\n", hashTable[position]->letter);
            }
            Num_Lives -= 1;
            Num_Victories = 0;
        }
        random_count = rand() % 36;
        set_correct_led();
        curr_input[0] = '\0';
        curr_morse_code2[0] = '\0';
        if (correct_input + incorrect_input)
        {
            statistics_table(0);
        } 
        
    }

    statistics_table(1);
    if (Num_Lives == 0) 
    {
        set_LED_red();
    }

    printf("\n+-----------------------------------------------------------------------------------------+\n");
    printf("\n                               Enter ----- to restart the game                             \n");
    printf("\n                               Enter .---- to exit                                         \n");
    printf("\n+-----------------------------------------------------------------------------------------+\n");
    curr_index = -1;
    main_asm();

    if (curr_index == 0) 
    {
        printf("\n No update provided program exiting \n");
    }
    if (strcmp(curr_input, "-----") == 0) 
    {
        learn_morse();
    }

}

// Frre memory allocated to the hash Table.

void deadloc()
{
    for(int i = 0; i < ARRAY_SIZE; i++)
    {
        free(hashTable[i]);
    }
    for(int i = 0; i < 36; i++)
    {
        free(Random_Letter[i]);
    }
}

// Main entry point for the code - simply calls the main assembly function.

int main() 
{
    stdio_init_all();
    PIO pio = pio0;
    uint offset = pio_add_program(pio, &ws2812_program);
    ws2812_program_init(pio, 0, offset, WS2812_PIN, 800000, IS_RGBW);
    RandomPermutation(); 
    insert();
    watchdog_enable(0x7fffff, 1);
    learn_morse();
    deadloc();
    printf("\n+-----------------------------------------------------------------------------------------+\n");
    printf("\n                                          GAME OVER                                        \n");
    printf("\n+-----------------------------------------------------------------------------------------+\n");
    return(0);
}
